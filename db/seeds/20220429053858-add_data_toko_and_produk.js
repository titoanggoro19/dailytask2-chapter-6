"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "Tokos",
      [
        {
          id: 1,
          namatoko: "Toko Musik Nirwana",
          alamat: "Perum Grand Teratai, Sidoarjo",
          owner: "John WIck",
          notelepon: "081230898745",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          namatoko: "Toko Musik N-One",
          alamat: "Perum Jenggolo Asri, Sidoarjo",
          owner: "Ariel Noah",
          notelepon: "081230888846",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );

    await queryInterface.bulkInsert("Produks", [
      {
        IDtoko: 1,
        merk: "Gibson",
        kategori: "Gitar",
        harga: 65000000,
        stok: 2,
        deskripsi: "Barang keren nih bro",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        IDtoko: 2,
        merk: "Yamaha",
        kategori: "Gitar",
        harga: 6000000,
        stok: 7,
        deskripsi: "Barang keren nih bro",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Tokos", null, {});
    await queryInterface.bulkDelete("Produks", null, {});
  },
};
