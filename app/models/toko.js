"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Toko extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Toko.init(
    {
      namatoko: DataTypes.STRING,
      alamat: DataTypes.STRING,
      owner: DataTypes.STRING,
      notelepon: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Toko",
    }
  );

  Toko.associate = function (models) {
    //association can be defined here
    Toko.hasMany(models.Produk, {
      foreignKey: "IDtoko",
    });
  };
  return Toko;
};
