"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Produk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Produk.init(
    {
      IDtoko: DataTypes.INTEGER,
      merk: DataTypes.STRING,
      kategori: DataTypes.STRING,
      harga: DataTypes.INTEGER,
      stok: DataTypes.INTEGER,
      deskripsi: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Produk",
    }
  );

  Produk.associate = function (models) {
    //association can be defined here
    Produk.belongsTo(models.Toko, {
      foreignKey: "IDtoko",
    });
  };

  return Produk;
};
