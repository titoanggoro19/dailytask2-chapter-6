/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const ProdukService = require("../../../services/produk");

module.exports = {
  list(req, res) {
    ProdukService.getAllProduk()
      .then((produk) => {
        res.status(201).json({
          status: "OK",
          data: produk,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { IDtoko, merk, kategori, harga, stok, deskripsi } = req.body;
    ProdukService.createProduk(IDtoko, merk, kategori, harga, stok, deskripsi)
      .then((produk) => {
        res.status(201).json({
          status: "OK",
          data: produk,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const produk = req.produk;
    ProdukService.updateProduk(produk, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: produk,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const produk = req.produk;

    res.status(200).json({
      status: "OK",
      data: produk,
    });
  },

  destroy(req, res) {
    ProdukService.deleteProduk(req.produk)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setProduk(req, res, next) {
    ProdukService.findProdukPK(req.params.id)
      .then((produk) => {
        if (!produk) {
          res.status(404).json({
            status: "FAIL",
            message: "Produk not found!",
          });

          return;
        }

        req.produk = produk;

        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Produk not found!",
        });
      });
  },
};
