/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const { Produk } = require("../../../models");
const TokoService = require("../../../services/toko");

module.exports = {
  list(req, res) {
    TokoService.getAllToko({
      include: {
        model: Produk,
        attributes: [
          "IDtoko",
          "merk",
          "kategori",
          "harga",
          "stok",
          "deskripsi",
        ],
      },
    })
      .then((toko) => {
        res.status(201).json({
          status: "OK",
          data: toko,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    const { namatoko, alamat, owner, notelepon } = req.body;
    TokoService.createToko(namatoko, alamat, owner, notelepon)
      .then((toko) => {
        res.status(201).json({
          status: "OK",
          data: toko,
        });
      })
      .catch((err) => {
        res.status(201).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const toko = req.toko;
    TokoService.updateToko(toko, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: toko,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const toko = req.toko;

    res.status(200).json({
      status: "OK",
      data: toko,
    });
  },

  destroy(req, res) {
    TokoService.deleteToko(req.toko)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setToko(req, res, next) {
    TokoService.findTokoPK(req.params.id)
      .then((toko) => {
        if (!toko) {
          res.status(404).json({
            status: "FAIL",
            message: "Toko not found!",
          });

          return;
        }

        req.toko = toko;

        next();
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Toko not found!",
        });
      });
  },
};
