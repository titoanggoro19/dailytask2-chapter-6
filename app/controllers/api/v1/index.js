/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const post = require("./post");
const toko = require("./toko");
const produk = require("./produk");

module.exports = {
  post,
  toko,
  produk,
};
