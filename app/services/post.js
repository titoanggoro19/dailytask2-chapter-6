const { Post } = require("../models");

module.exports = {
  getAllPosts() {
    const posts = Post.findAll();
    return posts;
  },

  createPosts(title, body) {
    const posts = Post.create({
      title,
      body,
    });
    return posts;
  },

  updatePost(oldpost, newpost) {
    return oldpost.update(newpost);
  },

  deletePost(oldpost) {
    return oldpost.destroy();
  },

  findPostPK(findpost) {
    return Post.findByPk(findpost);
  },
};
