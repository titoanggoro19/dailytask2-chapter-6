const { Produk } = require("../models");

module.exports = {
  getAllProduk() {
    const produk = Produk.findAll();
    return produk;
  },

  createProduk(IDtoko, merk, kategori, harga, stok, deskripsi) {
    const produk = Produk.create({
      IDtoko,
      merk,
      kategori,
      harga,
      stok,
      deskripsi,
    });
    return produk;
  },

  updateProduk(oldproduk, newproduk) {
    return oldproduk.update(newproduk);
  },

  deleteProduk(oldproduk) {
    return oldproduk.destroy();
  },

  findProdukPK(findproduk) {
    return Produk.findByPk(findproduk);
  },
};
