const { Toko } = require("../models");

module.exports = {
  getAllToko(a) {
    const toko = Toko.findAll(a);
    return toko;
  },

  createToko(namatoko, alamat, owner, notelepon) {
    const toko = Toko.create({
      namatoko,
      alamat,
      owner,
      notelepon,
    });
    return toko;
  },

  updateToko(oldtoko, newtoko) {
    return oldtoko.update(newtoko);
  },

  deleteToko(oldtoko) {
    return oldtoko.destroy();
  },

  findTokoPK(findTokoPK) {
    return Toko.findByPk(findTokoPK);
  },
};
